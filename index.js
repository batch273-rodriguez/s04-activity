// source: https://codeshare.io/EBEn18

class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}


// Class to represent a section of students

class Section {

    //every Section object will be instantiated with an empty array for its students
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
    }

    // Method for adding student to this section
    // Thi will take in the same arguments needed to instantiate a Student object
    addStudent(name,email,grades) {
        //a Student object will be instantiated before being pushed to the students property
        this.students.push(new Student(name, email,grades));

        //return Section object afterwards, allowing us to chain this method
        return this;
    }

    // Method for computing how many students in the section are honor students
    countHonorStudents() {
        let count = 0;
        this.students.forEach(student => {

            if (student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
            // console.log(count);
        })

        this.honorStudents = count;
        return this;
    }

    computeHonorsPercentage() {
        
        this.honorsPercentage = this.countHonorStudents().honorStudents/this.students.length*100;
        return this;

    }

}


// New Activity - Function Coding
class Grade {

    constructor (number) {
        this.level = number;
        this.sections = []
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMinGrade = undefined;
        this.batchMaxGrade = undefined;
    }
    
    addSection (name) {
        this.sections.push(new Section(name));
        return this;
    }

    countStudents() { //number of students in multiple sections
        this.totalStudents = 0;
        this.sections.forEach((section) => {
            // console.log(section.students);
            section.students.forEach((student) => {
                this.totalStudents++;
            });
            // console.log(this.totalStudents);
        });
        return this;
    }

    countHonorStudents() { //number of honor students ONLY in multiple sections
        this.totalHonorStudents = 0;
        this.sections.forEach((section) => {
            // console.log(section.students);
            section.students.forEach((student) => {
                if (student.computeAve().willPass().willPassWithHonors().passedWithHonors){
                    this.totalHonorStudents++;
                }
            });
            // console.log(this.totalStudents);
        });
        return this;
    }

    computeBatchAve() {
        let gradeSum = 0;
        this.sections.forEach((section) => {
            section.students.forEach((student)=>{
                gradeSum = gradeSum + student.computeAve().gradeAve;
                
            })
        })
        this.batchAveGrade = (gradeSum / this.countStudents().totalStudents);
        return this;

    }

    getBatchMinGrade() {
        let lowestGrade = -1;
        this.sections.forEach((section)=>{
            section.students.forEach((student)=>{
                // console.log(student);
                // console.log(lowestGrade);
                student.grades.forEach((grade)=>{
                        if(lowestGrade <0){
                            lowestGrade=grade;
                        }
                        else if(grade<lowestGrade){
                            lowestGrade=grade;
                        }
                })
            })
        })
        this.batchMinGrade = lowestGrade;
        return this;
    }

    getBatchMaxGrade() {
        let highestGrade = -1;
        this.sections.forEach((section)=>{
            section.students.forEach((student)=>{
                // console.log(student);
                // console.log(lowestGrade);
                student.grades.forEach((grade)=>{
                        if(highestGrade <0){
                            highestGrade=grade;
                        }
                        else if(grade>highestGrade){
                            highestGrade=grade;
                        }
                })
            })
        })
        this.batchMaxGrade = highestGrade;
        return this;
    }

}

//-----------for testing--------------//
//instantiate new Grade object
const grade1 = new Grade(1);

//2. add sections to this grade level
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');

//save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

//populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);
